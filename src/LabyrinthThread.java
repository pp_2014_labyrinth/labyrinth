import java.io.Serializable;



public class LabyrinthThread implements Runnable,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final LabyrinthManager lm;
	final Point start;
	final Point end;
	final ImmutableList<Point> p;
	public LabyrinthThread(LabyrinthManager lm, Point start, Point end, ImmutableList<Point> p) {
		this.lm = lm;
		this.start = start;
		this.end = end;
		this.p = p;
	}
	
	@Override
	public void run() {
		lm.calcRoute(start, end, p);
	}
}
