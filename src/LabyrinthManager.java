

import java.util.ArrayDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class LabyrinthManager {
	Labyrinth l;
    Point[] solution;
    private static LabyrinthManager instance = null;
    private final ExecutorService pool;
    

	public LabyrinthManager() {
    	//pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    	pool = new ThreadPoolExecutor(4,16, 1,
				TimeUnit.MILLISECONDS, new LinkedBlockingDeque(){

			@Override
			public Object take() throws InterruptedException {
				// TODO Auto-generated method stub
				return super.takeLast();
			}
    	});
    	instance = this;
	}
	public Point[] solveParallel(Labyrinth l) {
		//Das generierte Labyrinth abspeichern.
		this.l = l;
		
		//Der aktuelle Punkt ist der generierte Startpunkt
		Point current = l.getStart();
		
		//Starte das Berechnen der Route
		pool.submit(new LabyrinthThread(this, current, l.getEnd(), null));

		synchronized (instance){
            try {
            	//Warte bis das Ergbnis da ist
            	
            	instance.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
		
		//Gib Ergebnis zurück, wenn es fertig berechnet ist
		return solution;
	}
	/**
	 * Berechne die Route
	 * @param start Startpunkt der Route
	 * @param end Endpunkt der Route
	 * @param p Der bisher gegangene Weg
	 * @param b Der nächste Punkt mit der jeweiligen Richtung in die es weiter gehen soll
	 */
	public void calcRoute(Point start, Point end, ImmutableList<Point> p){
		
		//Der aktuelle Punkt ist der Startpunkt
		Point current = start;
		
		//Inidkator ob die aktuelle Route die Ergebnisroute ist
		boolean isTheRoute = true;
		
		ArrayDeque<PointAndDirection> backtrackStack = new ArrayDeque<PointAndDirection>();
		
		//Falls ein "bisher gegangener"-Weg übergeben wurde diesen beachten
		//Ansonsten von vorne beginnen
		ImmutableList<Point> pathSoFar;
		if(p == null)
			pathSoFar = new ImmutableList<Point>();
		else{
			pathSoFar = p;
		}
				
		//Solange der aktuelle Punkt noch nicht der Endpunkt ist verarbeitung fortsetzen
		while (!current.equals(end)) {
			Point next = null;
			
			//Festhalten, dass der aktuelle Punkt bereits besucht wurde
			l.visit(current);

			//Alle Richtungen überprüfen
			Direction[] dirs = Direction.values();
			for (Direction directionToNeighbor: dirs) {
				//Der bei dieser Iteration betrachtete Nachbar
				final Point neighbor = current.getNeighbor(directionToNeighbor);
				
				//Wenn der betrachtete Nachbar noch nicht vorher besucht wurde
				//Und wenn es einen Durchgang zwischen dem aktuellen Punkt und Nachbarn gibt
				//Sollte der aktuelle Punkt noch nicht notiert worden sein dies jetzt tun
				if (l.hasPassage(current, neighbor) && !l.visitedBefore(neighbor)) {
					
					//Alle Variablen zwischenspeichern
					final Point e = end;
					final ImmutableList<Point> psf = pathSoFar;
					if(next != null){
						pool.submit(new LabyrinthThread(this,neighbor, e, psf.add(current)));
						continue;
					}
					
					//Wenn es noch keinen nächsten gibt nimm den Nachbarn
					//Wenn es schon einen gibt starte eine neue Routenberechnung von hier
					next = neighbor;
				}
			}
			if (next != null) {
				//Der Weg geht noch weiter
				pathSoFar = pathSoFar.add(current);
				current = next;
			} else { 
				if (backtrackStack.isEmpty()){
					isTheRoute = false;
					
					break;
				}
				
				// Backtrack: Continue with cell saved at latest branching point:
				PointAndDirection pd = backtrackStack.pop();
				current = pd.getPoint();
				Point branchingPoint = current.getNeighbor(pd.getDirectionToBranchingPoint());

				// Remove the dead end from the top of pathSoFar, i.e. all cells after branchingPoint:
				while (!pathSoFar.getLast().equals(branchingPoint)) {
					pathSoFar = pathSoFar.removeLast();
				}	
			}
			
		}
		//long endMilli = Calendar.getInstance().getTimeInMillis();
		//System.out.println(endMilli-startMilli+"|"+i);
		//Ist dies die Ergebnisroute?
		if(isTheRoute){
			
			
			//Den aktuellen Punkt anfügen an die Route
			final ImmutableList<Point> psf = pathSoFar.add(current);
			
			//Die instance benachrichtigen, dass das Ergebnis da ist
			synchronized (instance) {
				Point[] sol = new Point[psf.getSize()];
				{
					int i = psf.getSize() -1;
					for (Point point : psf)
					{
						sol[i] = point;
						--i;
					}
				}
				
				
				solution = sol;
				instance.notify();
			}
			
		}
	}
}
