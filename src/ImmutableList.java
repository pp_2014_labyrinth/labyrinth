

import java.util.Iterator;

public class ImmutableList<T> implements Iterable<T>
{
	private Node<T> _first;
	private Node<T> _last;
	private int _size = 0;
	
	public ImmutableList()
	{
		this._first = null;
		this._last = null;
	}
	
	private ImmutableList(Node<T> first, Node<T> last, int size)
	{
		this._first = first;
		this._last = last;
		this._size = size;
	}

	public final T getFirst()
	{
		return this._first.get();
	}

	public final T getLast()
	{
		return this._last.get();
	}
	
	public ImmutableList<T> add(T item)
	{	
		if (this._first == null)
		{
			Node<T> n = new Node<T>(null, item);
			return new ImmutableList<T>(n, n, 1);
		} else {
			Node<T> newNode = new Node<T>(_last, item);
			ImmutableList<T> iL = new ImmutableList<T>(_first, newNode, _size+1);
			return iL;
		}	
	}

	public final ImmutableList<T> removeLast()
	{
		return new ImmutableList<T>(_first, _last.before(), _size-1);
	}
	
	public int getSize()
	{
		return this._size;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>()
		{
			Node<T> next = _last;
			Node<T> lastReturned = null;
			int nextIndex = 0;
			
			@Override
			public boolean hasNext()
			{
				return nextIndex < _size;
			}

			@Override
			public T next()
			{
				lastReturned = next;
				next = next.before();
				++nextIndex;
				return lastReturned.get();
			}

			@Override
			public void remove()
			{
				
			}
		};
	}
}