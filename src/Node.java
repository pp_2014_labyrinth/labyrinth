

public class Node<E>
{
	private Node<E> _before;
	private E _item;
	
	public Node(Node<E> before, E item)
	{
		_before = before;
		_item = item;
	}
	
	public final E get()
	{
		return this._item;
	}
	
	public final Node<E> before()
	{
		return this._before;
	}
}